## C# Lambda表达式

lambda表达式是一个未命名的方法，代替一个委托实例。

给定以下委托类型：

```
delegate int Converter (int i);
```

我们可以如下分配和调用lambda表达式x => x * x：

```
Converter sqr = x => x * x;
Console.WriteLine (sqr(3)); // 9
```

lambda表达式具有以下形式：

```
(parameters) => expression
We can omit the parentheses if and only if there is exactly one
parameter of an inferable type.
```

在我们的示例中，有一个参数，` x `，表达式为` x * x `:

```
x => x * x;
```

lambda表达式的每个参数都对应一个委托参数，表达式的类型对应于委托的返回类型。

在上面的代码中，` x `对应于参数` i `，表达式` x * x `对应于返回类型` int `，因此它与` Converter `委托兼容，如下所示：

```
delegate int Converter (int i);
```

lambda表达式的代码可以是语句块而不是表达式。

```
x => { return x * x; };
```

Lambda表达式最常用于Func和Action委托。

我们可以重写上面的代码如下：

```
Func<int,int> sqr = x => x * x;
```

下面是一个接受两个参数的表达式示例：

```
Func<string,string,int> totalLength = (s1, s2) => s1.Length + s2.Length;
int total = totalLength ("a", "the"); 
```

## 捕获外部变量

lambda表达式可以引用定义它的方法的局部变量和参数。

例如:

```
static void Main(){
   int factor = 2;
   Func<int, int> multiplier = n => n * factor;
   Console.WriteLine (multiplier (3)); // 6
}
```

由lambda表达式引用的外部变量称为捕获变量。

捕获变量的lambda表达式称为闭包。

捕获的变量在实际调用委托时求值，而不是在捕获变量时求值：

```
int factor = 2;
Func<int, int> multiplier = n => n * factor;
factor = 3;
Console.WriteLine (multiplier (3)); // 9
```

Lambda表达式可以更新捕获的变量：

```
int outerVariable = 0;
Func<int> myLambda = () => outerVariable++;
Console.WriteLine (myLambda()); // 0
Console.WriteLine (myLambda()); // 1
Console.WriteLine (outerVariable); // 2
```